package com.Internity.model;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;
@Data

public class PostModal {
    private MultipartFile multipartFile;
    private String posDes;
}
