package com.Internity.controller;

import com.Internity.component.FileUploadHelper;
import com.Internity.dto.PostDTO;
import com.Internity.model.Post;
import com.Internity.model.User;
import com.Internity.service.PostService;
import com.Internity.service.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Base64;
import java.util.List;
import java.util.NoSuchElementException;

@Validated
@RestController
@RequestMapping("/post")
public class PostController {

    @Autowired
    private PostService postService;

    @Autowired
    private UserService userService;

    @Autowired
    private FileUploadHelper fileUploadHelper;


   /* @ApiOperation(value = "Upload new Post of Existing user")
    @PostMapping(path = "/{mobile}")
    public ResponseEntity<Object> createNewPost(@RequestPart("file") MultipartFile file, @RequestPart("description") String description, @PathVariable("mobile") long mobile) throws IOException {
        final String postEncodeToString = Base64.getEncoder().encodeToString(file.getBytes());
        Post post = Post.builder().postPic(postEncodeToString).description(description).build();
        User user = userService.findByMobile(mobile);
        if (user != null) {
            post.setUser(user);
            Post createdPost = postService.createNewPost(post);
            return ResponseEntity.ok(createdPost);
        }
        return new ResponseEntity<>("User not found", HttpStatus.FORBIDDEN);

       User userFetch = null;
        try {
            userFetch = userService.findByMobile(mobile);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            post.setUser(userFetch);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("Post Photo path is not correct");
        }

        try {
            long postId = postService.count() + 1;
            String postString = fileUploadHelper.uploadEncodedImage(post.getPostPic(), userFetch.getFullName() + "_" + "post_" + postId);
            post.setPostPic(postString);
            post = postService.createNewPost(post);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("Post Uploadation Failed!!!!!!");
        }

    }*/

    @ApiOperation(value = "Upload new Post of Existing user")
    @PostMapping(path = "/{mobile}")
    public ResponseEntity<Object> createNewPost(@RequestBody PostDTO postDTO, @PathVariable("mobile") long mobile) throws IOException {

        Post post = new Post(postDTO.getPostPic(),postDTO.getDescription());
        System.out.println(mobile);
        User userFetch = userService.findByMobile(mobile);;
//        try {
//            userFetch = userService.findByMobile(mobile);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        /*try {
            post.setUser(userFetch);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("Post Photo path is not correct");
        }*/

        try {
            post.setUser(userFetch);
            post = postService.createNewPost(post);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("Post Uploadation Failed!!!!!!");
        }
        return ResponseEntity.ok(post);
    }

    @ApiOperation(value = "Get all post Of Existing User")
    @GetMapping("/{mobile}")
    public ResponseEntity<Object> fetchAllPostOfUser(@PathVariable("mobile") long mobile) {

        User userFetch = null;
        try {
            userFetch = userService.findByMobile(mobile);
        } catch (Exception e) {
            throw new NoSuchElementException("User Not Exist");
        }


        List<Post> posts = null;

        try {
            posts = postService.fetchAllPostOfUser(userFetch);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Post not exist for particular user");
        }

        return ResponseEntity.ok(posts);
    }

    @ApiOperation(value = "Get Single post by PostId Of Existing User")
    @GetMapping("/{mobile}/{postId}")
    public ResponseEntity<Object> fetchPostOfUser(@PathVariable("mobile") long mobile, @PathVariable("postId") long postId) {

        User userFetch = null;

        try {
            userFetch = userService.findByMobile(mobile);
        } catch (Exception e) {
            throw new NoSuchElementException("User not registered");
        }

        Post post = postService.fetchSinglePostOfUserByPostId(postId, userFetch);
        if (post == null) {
            throw new NoSuchElementException("Post didn't uploaded with Id " + postId);
        }

        return ResponseEntity.ok(post);
    }


}
